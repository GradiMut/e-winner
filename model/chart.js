const mongoose = require('mongoose');

const chartScehma = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    itemId: {type: String, require},
    userId: {type: String, require}
});

let chartM = module.exports = mongoose.model('chart', chartScehma);
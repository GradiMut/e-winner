const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    productName: {type: String, require},
    category: {type: String, require},
    price: {type: Number, require},
    description: {type: String, require},
    imageUrl: {type: String, require},
    userId: {type: String},
    dateTime: {type: Date}
});

let products = module.exports = mongoose.model('products', productSchema)
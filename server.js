// jshint esversion: 6
const express = require('express');
const app = express();
const fileUpload = require('express-fileupload')
const router = require('./routes/index');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');



// Parse incomming request
app.use(fileUpload({useTempFiles: true}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(router);

app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


// configuring the database
const dbConfig = require('./config/database.config');
const mongoose = require('mongoose');

// ROuter called

async function connectToDB () {
    try {
        await mongoose.connect(dbConfig.devUrl, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        });

        console.log("Successfully connected to the db");

    } catch (err)  {
        console.log("Could not connect to the database : ", err);
    } 
}

connectToDB();

const PORT = process.env.PORT || 3000;




app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
})



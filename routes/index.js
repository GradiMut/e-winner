const express = require('express');
const productController = require('../controllers/productController');
const userController = require('../controllers/userController');
const chartController = require('../controllers/chartController')
const multipart = require('connect-multiparty');
const auth = require('../middleware/auth');

const router = express.Router();

// product or item router
router.post('/v1/addNewProduct', productController.addItem);
router.patch('/v1/updateItem/:id', productController.updateItem);
router.get('/v1/getAllItems', productController.getAllItem);
router.get('/v1/getItemById/:id', productController.getItemById);
router.get('/v1/getItemByCategory/:category', productController.getItemByCategory); 
router.delete('/v1/deleteItem/:id', productController.deleteItem);

// user router
router.post('/v1/signUp', userController.signUp);
router.post('/v1/signUpAdmin', userController.signUpAdmin);
router.post('/v1/logIn', userController.login);
router.get('/v1/getAllUser', userController.getAllUser);
router.get('/v1/getUserById/:id', userController.getUserById);
router.patch('/v1/updateUser/:id', userController.updateUser);
router.delete('/v1/deleteUser/:id', userController.deleteUser);


// charrouter
router.post('/v1/addCard', chartController.addToChart);
router.get('/v1/getChart/:userId', chartController.getChart);
router.delete('/v1/removeChart/:id', chartController.removeToChart);



module.exports = router;
# E-COMMERCE API

API build with node expres mongoDB JWT

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

To install the software on your local machine, you need first to clone the repositrory or download the zip file.

## Installing

The installation of this application is fairly straightforward, After cloning this repository to your local machine,CD into the package folder using your terminal and run the following command : 

```> npm install```

It will install the node_modules which will help you run the project on your local machine.

## Starting the server

```> npm start```

## Run the test

```> Not yet settup```

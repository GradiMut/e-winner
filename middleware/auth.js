const jwt = require('jsonwebtoken');
const User = require('../model/users');

// const auth = async (req, res, next) => {
//     try {
//         const header = req.headers.authorization;

//         if (!header || header === '') {
//             return res.status(401).send({
//                 status: 401,
//                 error: 'Unauthorized',
//             });
//         }


//         const token = jwt.verify(header, 'secret');
//         req.user = token;
//         next();
//     } catch (e) {
//         return res.status(401).send({
//             status: 401,
//             error: e,
//         });
//     }
// };

const auth = async(req, res, next) => {
    const token = req.header.authorization;

    try {
        const data = jwt.verify(token, 'secret');

        const user = await User.findOne({ _id: data._id, 'tokens.token': token })

        console.log("FInd ", user)

        if (!user) {
            throw new Error();
        }


        req.user = user
        req.token = token
        next()
    } catch (error) {
        res.status(401).send({ error: 'Not authorized to access this resource' })
    }

}
module.exports = auth
const chartModel = require('../model/chart');
const mongoose = require('mongoose');


class chart {
    async addToChart(req, res) {
        try {
            let chartData = new chartModel();

            chartData._id = new mongoose.Types.ObjectId();
            chartData.itemId = req.body.itemId;
            chartData.userId = req.body.userId;
            
            await chartData.save();
            res.status(200).json({
                message: "Added to chart ",
                data: chartData,
            });
        } catch(error) {
            res.status(400).json({
                message: "Error occured : " + error,
                error: error,
            })
        }
    };

    getChart(req, res) {
                    console.log("Chart : ", req.params);

        chartModel.find({userId: req.params.userId}, (err, item) => {
            if(item) { 
                return res.status(200).json({
                    message: "Success",
                    item: item
                });
            } else {
                return res.status(404).json({
                    message: err,
                });
            }
        });
        // then(chart => {
        //     res.status(200).json({
        //         message: "Successfully retrieved all items!",
        //         item: chart
        //     })
        // }).catch(error => {
        //     res.status(400).json({
        //         message: error,
        //     });
        // })
    };
    
    removeToChart(req, res) {
        chartModel.deleteOne({
            _id: req.params.id
        }).then(chart => {
            if (chart) {
                console.log("Check success ", chart);
            } else {
                console.log("error occur");
            }
            res.status(200).json({
                message: 'Delete success',
                data: chart
            });
        }).catch(err => {
            res.status(400).json({
                message: 'Error while deleting',
            });
        })
    };
}

const chartC = new chart();
module.exports = chartC
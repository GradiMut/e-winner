const User = require('../model/users');
const auth = require('../middleware/auth')

class user {
    async signUp(req, res) {
        try {
            const user = new User(req.body);
            user.role = "suscriber"
            await user.save();
            console.log("User : ", user);
            const token = await user.generateAuthToken();
            res.status(201).json({
                message: "Singup complete",
                user: user,
                token: token
            });
          } catch (error) {
              res.status(400).json({
                  message: "Error occur",
                  data: error
              });
          }
    };
    
    async signUpAdmin(req, res) {
        try {
            const user = new User(req.body);
            user.role = "admin";
            await user.save();
            const token = await user.generateAuthToken();
            res.status(201).json({
                message: "Singup complete",
                user: user,
                token: token
            });
          } catch (error) {
              res.status(400).json({
                  message: "Error occur",
                  data: error
              });
          }
    };

    async login(req, res) {
        try {
            const {email, password} = req.body;
            const user = await User.findByCredentials(email, password);

            if (!user) {
                return res.status(401).json({
                    error: 'Login failed! Check authentication credentials'
                });
            } else {
                const token = await user.generateAuthToken()
                res.status(200).json({
                    message: "Succes login",
                    data: user,
                    token: token
                })
            }
        }catch(error) {
            res.status(404).json({
                message: "Email or Password incorect"
            })
        }
    };

    getAllUser(req, res) {
        User.find().then(user => {
            res.status(200).json({
                message: "Successfully retrieved all items!",
                data: user
            })
        }).catch(error => {
            res.status(400).json({
                message: error,
            });
        })
    };

    getUserById(req, res) {
        User.findById(req.params.id, (err, user) => {
            if (user) {
                return res.status(200).json({
                    message: "Success",
                    data: user
                })
            } else {
                return res.status(404).json({
                    message: "User not find",
                    error: err
                })
            }
        });
    };

    updateUser(req, res) {
        User.updateOne({_id: req.params.id}, {$set: req.body}).then(users => {
            res.status(200).json({
                message: 'Update success',
                data: users
            });
        }).catch(err => {
            res.status(404).json({
                message: 'Product item not found',
                err: err
            });
        })
    };

    deleteUser(req, res) {
        User.deleteOne({_id: req.params.id}).then(users => {
            if (users) {
                console.log("Check success ", users);
            } else {
                console.log("error occur");
            }
            res.status(200).json({
                message: 'Delete success',
                data: users
            });
        }).catch(err => {
            res.status(400).json({
                message: 'Error while deleting',
            });
        })
    };
    

    
}

const userController = new user();

module.exports = userController;
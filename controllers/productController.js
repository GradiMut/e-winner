const mongoose = require('mongoose');

const cloudinary = require('cloudinary').v2;
const producs = require('../model/items');

const date = new Date();
const currentdate = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

cloudinary.config({
    cloud_name: 'dy1wdrede',
    api_key: '646471142728668',
    api_secret: 'BLPn82zuWZ9bQyNKDHLOTQIV0Lg',
});


class product {
 
    addItem(req, res, next) {      
        const imgFile = req.files.imageUrl;
  
        cloudinary.uploader.upload(imgFile.tempFilePath, function (errs, result){
            if(errs) {
                res.status(400).json({ 
                    message: errs,
                });
            } else {
                let products = new producs();

                products._id = new mongoose.Types.ObjectId();
                products.productName = req.body.productName;
                products.category = req.body.category;
                products.price = req.body.price;
                products.description = req.body.description;
                products.imageUrl = result.url;
                products.userId = req.body.userId;
                products.dateTime = currentdate;

                products.save(function (err) {
                    if (err) {
                        res.status(400).json({
                            message: err,
                        });
                     } else {
                            res.status(200).json({
                                message: "Item added to the database successfully!",
                                item: products
                            });
                        }
                });

            }
        });
            
    };

    getAllItem(req, res) {
        producs.find().then(products => {
            res.status(200).json({
                message: "Successfully retrieved all items!",
                item: products
            })
        }).catch(error => {
            res.status(400).json({
                message: error,
            });
        })
    };

    getItemById(req, res) {
        producs.findById(req.params.id, (err, item) => {
            if(item) {
                return res.status(200).json({
                    message: "Success",
                    item: item
                })
            } else {
                return res.status(404).json({
                    message: "Post not find",
                    error: err
                })
            }
        });
    }; 
    
    getItemByCategory(req, res) {

        producs.find({category: req.params.category}, (err, item) => {

            if(item) { 
                return res.status(200).json({
                    message: "Success",
                    item: item
                });
            } else {
                return res.status(404).json({
                    message: err,
                });
            }
        });
    };

    updateItem(req, res, next) {
        producs.updateOne({_id: req.params.id}, {$set: req.body}).then(items => {
            console.log('i : ', items);
            res.status(200).json({
                message: 'Update success',
                item: items
            });
        }).catch(err => {
            res.status(404).json({
                message: 'Product item not found',
                item: err
            });
        })
    };
    
    deleteItem(req, res) {
        producs.deleteOne({_id: req.params.id}).then(items => {
            if (items) {
                console.log("Check success ", items);
            } else {
                console.log("error occur");
            }
            res.status(200).json({
                message: 'Delete success',
                item: items
            });
        }).catch(err => {
            res.status(400).json({
                message: 'Error while deleting',
                item: err
            });
        })
    };
}

const productController = new product();

module.exports = productController;